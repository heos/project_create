#!/usr/bin/python

import ConfigParser
import argparse
import hashlib
import logging
import os
import paramiko
import shlex
import shutil
import subprocess
import sys
import traceback
import yaml


def clone_prj(user, host, port, path, prj):
    '''Function to clone project from the gerrit'''

    path = os.path.join(path, prj.split('/')[1])

    if os.path.isdir(path):
        try:
            shutil.rmtree(path)
            os.makedirs(path)
        except Exception as e:
            logging.error('Error: {}'.format(e))
            logging.error(traceback.print_exc(e))

    cmd = 'git clone ssh://{}@{}:{}/{} {}'.format(
        user,
        host,
        port,
        prj,
        path,
    )

    cmd = shlex.split(cmd)

    try:
        cmd = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE).communicate()
        print '123'
        print os.listdir(path)

    except Exception as e:
        logging.error('Cloning error of the {} project'.format(prj))
        logging.error('Error: {}'.format(e))
        logging.error(traceback.print_exc(e))


def parse(path):
    '''Function to parse the projects.yaml file'''

    path = os.path.join(path, 'projects.yaml')

    try:
        with open(path, 'r') as f:
            data = yaml.safe_load(f)
        f.close()

        return data
    except Exception as e:
        logging.error("Reading error of the '{}' config".format(path))
        logging.error('Error: {}'.format(e))
        logging.error(traceback.print_exc(e))
        sys.exit(1)


def gerrit_data(user, host, port):
    '''Function to recieve projects list from the gerrit'''

    gerrit = paramiko.SSHClient()
    gerrit.load_system_host_keys()
    gerrit.connect(host, port, user)

    cmd = 'gerrit ls-projects --type ALL'

    #Recieving list of the executed projects from the gerrit
    try:
        stdin, stdout, stderr = gerrit.exec_command(cmd)
        data = stdout.read().split('\n')
        logging.info('List of projects has been recieved from {}'.format(host))
        return data

    except Exception as e:
        logging.error('Unexpected error was returned when \
            transfered list of existing projects from the gerrit')
        logging.error('Error: {}'.format(e))
        logging.error(traceback.print_exc(e))
        sys.exit(1)


def copy_nacr(config, dirs, files, nacr):
    '''Function to copy NACR to a project '''

    user = config['user']
    host = config['host']
    port = config['port']

    # Get list of new projects
    new_prj = parse(dirs['dest'])
    projects = [i['project'] for i in new_prj]

    cmd_push = [
        'git add .',
        'git commit -a -m \'Added rules.pl file\'',
        'git push origin HEAD:refs/meta/config',
    ]

    src = os.path.join(dirs['nacrsrc'], files['nacr'])

    if os.path.exists(dirs['nacr']):
        shutil.rmtree(dirs['nacr'])
        os.mkdir(dirs['nacr'])

    for project in projects:
        if project.split('/')[0] in nacr:
            if '/' not in project:
                project = '%s.git' % (project)

            path = '%s/%s' % (dirs['nacr'], project)
            os.makedirs(path)
            os.chdir(path)

            cmd_get = [
                'git clone ssh://%s@%s:%s/%s %s' % (
                    user,
                    host,
                    port,
                    project,
                    path
                ),
                'git fetch origin refs/meta/config:config',
                'git checkout config',
            ]

            for j in cmd_get:
                try:
                    data = subprocess.Popen(
                        shlex.split(j),
                        stdout=subprocess.PIPE).communicate()[0]
                    logging.info(data)
                except Exception as e:
                    logging.error(
                        'Cloning error of the project {}'.format(project)
                    )
                    logging.error('Error: {}'.format(e))
                    logging.error(traceback.print_exc(e))

            dst = os.path.join(path, files['nacr'])

            if not os.path.exists(dst):
                shutil.copy2(src, path)
                for j in cmd_push:
                    try:
                        data = subprocess.Popen(
                            shlex.split(j),
                            stdout=subprocess.PIPE).communicate()[0]
                        logging.info(data)
                    except Exception as e:
                        logging.error(
                            'Pushing error of the {} project'.format(project)
                        )
                        logging.error('Error: {}'.format(e))
                        logging.error(traceback.print_exc(e))
            else:
                repo_hash = hashlib.md5(open(dst, 'r').read()).hexdigest()
                src_hash = hashlib.md5(open(src, 'r').read()).hexdigest()
                if repo_hash != src_hash:
                    try:
                        shutil.copy2(src, path)
                    except Exception as e:
                        logging.error(
                            'Copying error of {} file to {}'.format(src, path)
                        )
                        logging.error('Error: {}'.format(e))
                        logging.error(traceback.print_exc(e))

                    for j in cmd_push:
                        try:
                            data = subprocess.Popen(
                                shlex.split(j),
                                stdout=subprocess.PIPE).communicate()[0]
                            logging.info(data)
                        except Exception as e:
                            logging.error(
                                'Pushing error of the project {}'.format(
                                    project
                                )
                            )
                            logging.error('Error: {}'.format(e))
                            logging.error(traceback.print_exc(e))


def config_prepare(config, dirs, prj, files):
    ''' Function to prepare config file projects.yaml '''

    user = config['user']
    host = config['host']
    port = int(config['port'])

    # Cloning "project-config" project from the gerrit
#    for i in prj.keys():
#        clone_prj(user, host, port, dirs['tmp'], prj[i])

    # Getting projects list from the "projects.yaml" config
    conflist = parse(
        os.path.join(dirs['tmp'], prj['main'].split('/')[1]))
    projects = [i['project'] for i in conflist]

    # Getting projects list from the gerrit
    gerlist = gerrit_data(user, host, port)
    del gerlist[gerlist.index('All-Projects')]

    # Compute the mumber of the new projects as the difference
    # between "projects.yaml" config ang gerrit
    new_prj = set(projects) - set(gerlist)

    path = {
        i: os.path.join(dirs['dest'], files[i])
        for i in files.keys() if i != 'nacr'
    }

    # Creating the "projects.yaml" file with the new projects only
    # and the "projects.full.yaml" file with full list of projects
    #
    # If "new_prj" set is empty then will create the "projects.yaml" file only
    if len(new_prj) != 0:
        result = [j for j in conflist for i in new_prj if j['project'] == i]

        for i in path.keys():
            try:
                if not os.path.exists(path[i]):

                    if path[i] == path['new']:
                        with open(path[i], 'a') as f:
                            f.write(
                                yaml.dump(result, default_flow_style=False)
                            )
                        f.close()
                    else:
                        shutil.copy(
                            os.path.join(
                                dirs['tmp'],
                                prj['main'].split('/')[1],
                                files['new']
                            ),
                            path[i]
                        )
                else:
                    os.remove(path[i])
                    if path[i] == path['new']:
                        with open(path[i], 'a') as f:
                            f.write(yaml.dump(
                                result,
                                default_flow_style=False)
                            )
                        f.close()
                    else:
                        shutil.copy(
                            os.path.join(
                                dirs['tmp'],
                                dirs['tmp'],
                                prj['main'].split('/')[1],
                                files['new']
                            ),
                            path[i]
                        )
            except Exception as e:
                logging.error('Copying error of the new projects.yaml')
                logging.error('Error: {}'.format(e))
                logging.error(traceback.print_exc(e))
                sys.exit(1)
        logging.info(
            'New config file {} has bee copied to {}'.format(
                files['new'], path['new']
            )
        )

    else:
        try:
            if os.path.exists(path['full']):
                os.remove(path['full'])
            shutil.copy(
                os.path.join(
                    dirs['tmp'],
                    prj['main'].split('/')[1],
                    files['new']
                ),
                path['new']
            )
            logging.info(
                "Full file '{}' has been copied to {}".format(
                    files['new'], path['new'])
                )

        except Exception as e:
            logging.error('Copying error of the full projects.yaml file')
            logging.error('Error: {}'.format(e))
            logging.error(traceback.print_exc(e))
            sys.exit(1)

    try:
        shutil.copytree(
            os.path.join(dirs['tmp'], prj['main'].split('/')[1], 'acls'),
            os.path.join(dirs['dest'], 'acls')
        )
    except OSError as e:
        if e.errno == 17:
            shutil.rmtree(
                os.path.join(dirs['dest'], 'acls'),
                ignore_errors=True
            )
            shutil.copytree(
                os.path.join(dirs['tmp'], prj['main'].split('/')[1], 'acls'),
                os.path.join(dirs['dest'], 'acls')
            )
        else:
            logging.error("Copying error of the 'acls' dir")
            logging.error('Error: {}'.format(e))
            logging.error(traceback.print_exc(e))


def set_parent(config, dirs):
    ''' Function to set parent for project '''

    user = config['user']
    host = config['host']
    port = int(config['port'])

    # Get list with new projects
    new_prj = parse(dirs['dest'])
    projects = [i['project'] for i in new_prj]

    # Set a "parent" project to the new projects
    for project in projects:
        prnt = project.split('/')[0]
        cmd = 'gerrit set-project-parent --parent {} {}'.format(
            parents[prnt], project
        )

        try:
            gerrit = paramiko.SSHClient()
            gerrit.load_system_host_keys()
            gerrit.connect(host, port, user)

            stdin, stdout, stderr = gerrit.exec_command(cmd)
            logging.info(
                "Project '{}' was defined as parent for '{}' project".format(
                    parents[prnt], project)
            )
        except Exception as e:
            logging.error("Error definition of parent for {}".format(project))
            logging.error('Error: {}'.format(e))
            logging.error(traceback.print_exc(e))


def confparser(config_file='project_create.conf'):
    '''Function to parse the config file'''

    logging.basicConfig(
        level=logging.INFO,
        format=u'[%(asctime)s] --%(levelname)s-- %(message)s',
    )

    if os.path.exists(config_file):
        config = ConfigParser.ConfigParser()
        config.read(config_file)
        result = {}

        for i in config.sections():
            result[i] = {j: config.get(i, j) for j in config.options(i)}

        return result

    else:
        logging.error("Couldn't find config file in '{}'".format(config_file))
        sys.exit(1)


def main(config, dirs, prj, files, parents, nacr):

    logging.basicConfig(
        level=logging.INFO,
        format=u'[%(asctime)s] --%(levelname)s-- %(message)s',
    )

    parser = argparse.ArgumentParser(
        description='The tool to create and manage by new project in gerrit.'
    )
    parser.add_argument(
        '-m', '--make',
        action='store_true',
        help="make and copy new 'projects.yaml' file to the jeepyb's directory"
    )
    parser.add_argument(
        '-p', '--parent',
        action='store_true',
        help="set 'parent' project for the new projects"
    )
    parser.add_argument(
        '-n', '--nacr',
        action='store_true',
        help="add file with NACR-rules to the new projects"
    )
    parser.add_argument(
        '-j', '--jeepyb',
        action='store_true',
        help="run jeepyb from current script to create projects in gerrit"
    )

    args = parser.parse_args()

    # Creating of all directories
    for i in dirs.keys():
        if not os.path.exists(dirs[i]):
            try:
                os.mkdir(dirs[i])
                logging.info('The directory {} was created'.format(dirs[i]))
            except Exception as e:
                logging.error('Creating error of the dir {}'.format(dirs[i]))
                logging.error('Error: {}'.format(e))
                logging.error(traceback.print_exc(e))

    # Preparing of config file
    if args.make:
        config_prepare(config, dirs, prj, files)

    # Set "parent" project to new projects
    elif args.parent:
        set_parent(config, dirs)

    # Copy file with nacr-rules to new projects
    elif args.nacr:
        copy_nacr(config, dirs, files, nacr)

    # Run jeepyb to create new projects
    elif args.jeepyb:
        jeepyb_run(config, dirs, files, prj)


if __name__ == "__main__":

    data = confparser('/home/gerrit2/project_create/project_create.conf')

    config = data['main']
    dirs = data['dirs']
    prj = data['projects']
    files = data['files']
    parents = data['parents']
    nacr = [i.strip() for i in data['nacr'].values()[0].split(',')]

    main(config, dirs, prj, files, parents, nacr)
